#!/usr/bin/perl
# (c) Petr Baudis <pasky@suse.cz>
# GPLv2

use strict;
use warnings;

use lib ".";
use Girocco::CGI;
use Girocco::Config;
use Girocco::User;
use Girocco::Util;

my $gcgi = Girocco::CGI->new('User Registration');
my $cgi = $gcgi->cgi;

unless ($Girocco::Config::manage_users) {
	print "<p>I don't manage users.</p>";
	exit;
}

if ($cgi->param('mail')) {
	print "<p>Go away, bot.</p>";
	exit;
}

if ($cgi->param('name')) {
	# submitted, let's see
	# FIXME: racy, do a lock
	my $name = $gcgi->wparam('name');
	Girocco::User::valid_name($name)
		and Girocco::User::does_exist($name)
		and $gcgi->err("User with that name already exists.");

	my $user = Girocco::User->ghost($name);
	if ($user->cgi_fill($gcgi)) {
		$user->conjure;
		print <<EOT;
<p>User $name successfuly registered.</p>
<p>Project administrators can now give you push access to their projects.</p>
<p>Congratulations, and have a lot of fun!</p>
EOT
		exit;
	}
}

print <<EOT;
<p>Here you can register a user.
You need to register a user so that you can push to the hosted projects.</p>
<p>SSH is used for pushing (the <tt>ssh</tt> protocol), your SSH key authenticates you -
there is no password (though we recommend that your SSH key is password-protected;
use <code>ssh-agent</code> to help your fingers).
You can find your public key in <tt>~/.ssh/id_rsa.pub</tt> or <tt>~/.ssh/id_dsa.pub</tt>.
If you do not have any yet, generate it using the <code>ssh-keygen</code> command.
You can paste multiple keys in the box below, each on a separate line.
Paste each key <em>including</em> the <tt>ssh-</tt>whatever prefix and email-like postfix.</p>
<p>We won't bother to verify your email contact,
but fill in something sensible in your own interest
so that we can contact you or confirm your identity shall the need arise.
We also need to send you an e-mail if you want to update your SSH keys later.</p>
$Girocco::Config::legalese
<form method="post">
<table class="form">
<tr><td class="formlabel">Login:</td><td><input type="text" name="name" /></td></tr>
<tr><td class="formlabel">Email:</td><td><input type="text" name="email" /></td></tr>
<tr><td class="formlabel">Public SSH key(s):</td><td><textarea wrap="off" name="keys" rows="5" cols="80"></textarea></td></tr>
<tr style="display:none"><td class="formlabel">Anti-captcha (leave empty!):</td><td><input type="text" name="mail" /></td></tr>
<tr><td class="formlabel"></td><td><input type="submit" name="y0" value="Register" /></td></tr>
</table>
</form>
EOT
