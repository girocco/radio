#!/usr/bin/perl
# (c) Petr Baudis <pasky@suse.cz>
# (c) Jan Krueger <jk@jk.gs>
# GPLv2

use strict;
use warnings;

use lib ".";
use Girocco::CGI;
use Girocco::Config;
use Girocco::User;
use Girocco::Util;

my $gcgi = Girocco::CGI->new('User SSH Key Update');
my $cgi = $gcgi->cgi;

unless ($Girocco::Config::manage_users) {
	print "<p>I don't manage users.</p>";
	exit;
}

if ($cgi->param('mail')) {
	print "<p>Go away, bot.</p>";
	exit;
}

sub _auth_form {
	my $name = shift;
	my $submit = shift;
	my $fields = shift;
	$fields = '' if (!$fields);
	my $auth = shift;
	my $authtag = ($auth ? qq(<input type="hidden" name="auth" value="$auth" />) :
		qq(<p>Authorization code: <input name="auth" size="40" /></p>));
	print <<EOT;

<form method="post">
<input type="hidden" name="name" value="$name">
$authtag
$fields<p><input type="submit" value="$submit" /></p>
EOT
}

if ($cgi->param('name')) {
	# submitted, let's see
	# FIXME: racy, do a lock
	my $name = $gcgi->wparam('name');
	(Girocco::User::valid_name($name)
		and Girocco::User::does_exist($name))
		or $gcgi->err("Username is not registered.");

	$gcgi->err_check and exit;

	my $user = Girocco::User->load($name) or
		die "Failed loading user $name - but this can't really happen here";

	if (!$cgi->param('auth')) {
		my $auth = $user->gen_auth;

		# Send auth mail
		open(MAIL, '|-', '/usr/bin/mail', '-s', "[$Girocco::Config::name] Account update authorization", $user->{email}) or
			die "Sorry, could not send authorization code: $!";
		print MAIL <<EOT;
Hello,

you have requested an authorization code to be sent to you for updating your
account's SSH keys. If you don't want to actually update your SSH keys, just
ignore this e-mail. Otherwise, use this code within 24 hours:

$auth

Should you run into any problems, please let us know.

Have fun!
EOT
		close MAIL;

		print "<p>You should shortly receive an e-mail containing an authorization code.
			Please enter this code below to update your SSH keys.
			The code will expire in 24 hours or after you have used it.</p>";
		_auth_form($name, "'Login'");
		exit;
	} else {
		$user->{auth} or
			die("There currently isn't any authorization code filed under your account. Please <a href=\"edituser.cgi\">generate one</a>.");

		my $fields = '';
		my $keys = $cgi->param('keys') || '';
		if ($keys) {
			$fields = "<p>Public SSH key(s): <textarea wrap=\"off\" name=\"keys\" cols=\"80\" rows=\"5\">$keys</textarea></p>\n";
		}

		my $auth = $gcgi->wparam('auth');
		if ($auth ne $user->{auth}) {
			print '<p>Invalid authorization code, please re-enter or <a href="edituser.cgi">generate a new one</a>.</p>';
			_auth_form($name, "'Login'", $fields);
			exit;
		}

		# Auth valid, keys given -> save
		if ($keys) {
			$user->keys_fill($gcgi);
			$user->del_auth;
			$user->keys_save;
			print "<p>Your SSH keys have been updated.</p>";
			exit;
		}

		# Otherwise pre-fill keys
		$keys = $user->{keys};
		$fields = "<p>Public SSH key(s): <textarea name=\"keys\" cols=\"80\" rows=\"5\">$keys</textarea></p>\n";

		print "<p>Authorization code validated (for now).</p>
<p>You can paste multiple keys in the box below, each on a separate line.
Paste each key <em>including</em> the <tt>ssh-</tt>whatever prefix and email-like postfix.</p>\n";
		_auth_form($name, "Update keys", $fields, $auth);
		exit;
	}

}

print <<EOT;
<p>Here you can update the public SSH keys associated with your user account.
These keys are required for you to push to projects.</p>
<p>SSH is used for pushing (the <tt>ssh</tt> protocol), your SSH key authenticates you -
there is no password (though we recommend that your SSH key is password-protected;
use <code>ssh-agent</code> to help your fingers).
You can find your public key in <tt>~/.ssh/id_rsa.pub</tt> or <tt>~/.ssh/id_dsa.pub</tt>.
If you do not have any yet, generate it using the <code>ssh-keygen</code> command.</p>

<p>Please enter your username below;
we will send you an email with an authorization code
and further instructions.</p>

<form method="post">
<table class="form">
<tr><td class="formlabel">Login:</td><td><input type="text" name="name" /></td></tr>
<tr style="display:none"><td class="formlabel">Anti-captcha (leave empty!):</td><td><input type="text" name="mail" /></td></tr>
<tr><td class="formlabel"></td><td><input type="submit" value="Send authorization code" /></td></tr>
</table>
</form>
EOT
