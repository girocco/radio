package Girocco::Project;

use strict;
use warnings;

BEGIN {
	use Girocco::CGI;
	use Girocco::User;
	use Girocco::Util;
	use Girocco::ProjPerm;
	use Girocco::Config;
	use base ('Girocco::ProjPerm::'.$Girocco::Config::permission_control); # mwahaha
}

our $metadata_fields = {
	homepage => ['Homepage URL', 'hp', 'text'],
	shortdesc => ['Short description', 'desc', 'text'],
	README => ['README (HTML, lt 8kb)', 'README', 'textarea'],
	notifymail => ['Commit notify - mail to', 'notifymail', 'text'],
	notifyjson => ['Commit notify - <a href="http://help.github.com/post-receive-hooks/">POST JSON</a> at', 'notifyjson', 'text'],
	notifycia =>  ['Commit notify - <a href="http://cia.vc/doc/">CIA project</a> name', 'notifycia', 'text'],
};

sub _mkdir_forkees {
	my $self = shift;
	my @pelems = split('/', $self->{name});
	pop @pelems; # do not create dir for the project itself
	my $path = $self->{base_path};
	foreach my $pelem (@pelems) {
		$path .= "/$pelem";
		(-d "$path") or mkdir $path or die "mkdir $path: $!";
		chmod 0775, $path; # ok if fails (dir may already exist and be owned by someone else)
	}
}

our %propmap = (
	url => ':baseurl',
	email => ':owner',
	desc => 'description',
	README => 'README.html',
	hp => ':homepage',
	notifymail => '%hooks.mailinglist',
	notifyjson => '%hooks.jsonurl',
	notifycia => '%hooks.cianame',
);

sub _property_path {
	my $self = shift;
	my ($name) = @_;
	$self->{path}.'/'.$name;
}

sub _property_fget {
	my $self = shift;
	my ($name) = @_;
	my $pname = $propmap{$name};
	$pname or die "unknown property: $name";
	if ($pname =~ s/^://) {
		my $val = `git --git-dir="$self->{path}" config "gitweb.$pname"`;
		chomp $val;
		return $val;
	} elsif ($pname =~ s/^%//) {
		my $val = `git --git-dir="$self->{path}" config "$pname"`;
		chomp $val;
		return $val;
	}

	open P, $self->_property_path($pname) or return undef;
	my @value = <P>;
	close P;
	my $value = join('', @value); chomp $value;
	$value;
}

sub _property_fput {
	my $self = shift;
	my ($name, $value) = @_;
	my $pname = $propmap{$name};
	$pname or die "unknown property: $name";
	$value ||= '';
	if ($pname =~ s/^://) {
		system('git', '--git-dir='.$self->{path}, 'config', "gitweb.$pname", $value);
		return;
	} elsif ($pname =~ s/^%//) {
		system('git', '--git-dir='.$self->{path}, 'config', $pname, $value);
		return;
	}

	my $P = lock_file($self->_property_path($pname));
	$value ne '' and print $P "$value\n";
	close $P;
	unlock_file($self->_property_path($pname));
}

sub _properties_load {
	my $self = shift;
	foreach my $prop (keys %propmap) {
		$self->{$prop} = $self->_property_fget($prop);
	}
}

sub _properties_save {
	my $self = shift;
	foreach my $prop (keys %propmap) {
		$self->_property_fput($prop, $self->{$prop});
	}
}

sub _nofetch_path {
	my $self = shift;
	$self->_property_path('.nofetch');
}

sub _nofetch {
	my $self = shift;
	my ($nofetch) = @_;
	my $np = $self->_nofetch_path;
	if ($nofetch) {
		open X, '>'.$np or die "nofetch failed: $!";
		close X;
	} else {
		unlink $np or die "yesfetch failed: $!";
	}
}

sub _clonelog_path {
	my $self = shift;
	$self->_property_path('.clonelog');
}

sub _clonefail_path {
	my $self = shift;
	$self->_property_path('.clone_failed');
}

sub _clonep_path {
	my $self = shift;
	$self->_property_path('.clone_in_progress');
}

sub _clonep {
	my $self = shift;
	my ($nofetch) = @_;
	my $np = $self->_clonep_path;
	if ($nofetch) {
		open X, '>'.$np or die "clonep failed: $!";
		close X;
	} else {
		unlink $np or die "clonef failed: $!";
	}
}
sub _alternates_setup {
	my $self = shift;
	return unless $self->{name} =~ m#/#;
	my $forkee_name = get_forkee_name($self->{name});
	my $forkee_path = get_forkee_path($self->{name});
	return unless -d $forkee_path;
	mkdir $self->{path}.'/refs'; chmod 0775, $self->{path}.'/refs';
	mkdir $self->{path}.'/objects'; chmod 0775, $self->{path}.'/objects';
	mkdir $self->{path}.'/objects/info'; chmod 0775, $self->{path}.'/objects/info';

	# We set up both alternates and http_alternates since we cannot use
	# relative path in alternates - that doesn't work recursively.

	my $filename = $self->{path}.'/objects/info/alternates';
	open X, '>'.$filename or die "alternates failed: $!";
	print X "$forkee_path/objects\n";
	close X;
	chmod 0664, $filename or warn "cannot chmod $filename: $!";

	if ($Girocco::Config::httppullurl) {
		$filename = $self->{path}.'/objects/info/http-alternates';
		open X, '>'.$filename or die "http-alternates failed: $!";
		my $upfork = $forkee_name;
		do { print X "$Girocco::Config::httppullurl/$upfork.git/objects\n"; } while ($upfork =~ s#/?.+?$## and $upfork); #
		close X;
		chmod 0664, $filename or warn "cannot chmod $filename: $!";
	}

	# The symlink is problematic since git remote prune will traverse it.
	#symlink "$forkee_path/refs", $self->{path}.'/refs/forkee';
}

sub _ctags_setup {
	my $self = shift;
	mkdir $self->{path}.'/ctags'; chmod 01777, $self->{path}.'/ctags';
}

sub _group_add {
	my $self = shift;
	my ($xtra) = @_;
	$xtra .= join(',', @{$self->{users}});
	filedb_atomic_append(jailed_file('/etc/group'),
		join(':', $self->{name}, $self->{crypt}, '\i', $xtra));
}

sub _group_update {
	my $self = shift;
	my $xtra = join(',', @{$self->{users}});
	filedb_atomic_edit(jailed_file('/etc/group'),
		sub {
			$_ = $_[0];
			chomp;
			if ($self->{name} eq (split /:/)[0]) {
				# preserve readonly flag
				s/::([^:]*)$/:$1/ and $xtra = ":$xtra";
				return join(':', $self->{name}, $self->{crypt}, $self->{gid}, $xtra)."\n";
			} else {
				return "$_\n";
			}
		}
	);
}

sub _group_remove {
	my $self = shift;
	filedb_atomic_edit(jailed_file('/etc/group'),
		sub {
			$self->{name} ne (split /:/)[0] and return $_;
		}
	);
}

sub _hook_path {
	my $self = shift;
	my ($name) = @_;
	$self->{path}.'/hooks/'.$name;
}

sub _hook_install {
	my $self = shift;
	my ($name) = @_;
	open SRC, "$Girocco::Config::basedir/hooks/$name" or die "cannot open hook $name: $!";
	open DST, '>'.$self->_hook_path($name) or die "cannot open hook $name for writing: $!";
	while (<SRC>) { print DST $_; }
	close DST;
	close SRC;
	chmod 0775, $self->_hook_path($name) or die "cannot chmod hook $name: $!";
}

sub _hooks_install {
	my $self = shift;
	foreach my $hook ('post-receive', 'update', 'post-update') {
		$self->_hook_install($hook);
	}
}

# private constructor, do not use
sub _new {
	my $class = shift;
	my ($name, $base_path, $path) = @_;
	valid_name($name) or die "refusing to create project with invalid name ($name)!";
	$path ||= "$base_path/$name.git";
	my $proj = { name => $name, base_path => $base_path, path => $path };

	bless $proj, $class;
}

# public constructor #0
# creates a virtual project not connected to disk image
# you can conjure() it later to disk
sub ghost {
	my $class = shift;
	my ($name, $mirror) = @_;
	my $self = $class->_new($name, $Girocco::Config::reporoot);
	$self->{users} = [];
	$self->{mirror} = $mirror;
	$self;
}

# public constructor #1
sub load {
	my $class = shift;
	my ($name) = @_;

	open F, jailed_file("/etc/group") or die "project load failed: $!";
	while (<F>) {
		chomp;
		@_ = split /:+/;
		next unless (shift eq $name);

		my $self = $class->_new($name, $Girocco::Config::reporoot);
		(-d $self->{path}) or die "invalid path (".$self->{path}.") for project ".$self->{name};

		my $ulist;
		($self->{crypt}, $self->{gid}, $ulist) = @_;
		$ulist ||= '';
		$self->{users} = [split /,/, $ulist];
		$self->{orig_users} = [@{$self->{users}}];
		$self->{mirror} = ! -e $self->_nofetch_path;
		$self->{clone_in_progress} = -e $self->_clonep_path;
		$self->{clone_logged} = -e $self->_clonelog_path;
		$self->{clone_failed} = -e $self->_clonefail_path;
		$self->{ccrypt} = $self->{crypt};

		$self->_properties_load;
		return $self;
	}
	close F;
	undef;
}

# $proj may not be in sane state if this returns false!
sub cgi_fill {
	my $self = shift;
	my ($gcgi) = @_;
	my $cgi = $gcgi->cgi;

	my ($pwd, $pwd2) = ($cgi->param('pwd'), $cgi->param('pwd2'));
	$pwd ||= ''; $pwd2 ||= ''; # in case passwords are disabled
	if ($pwd or not $self->{crypt}) {
		$self->{crypt} = scrypt($pwd);
	}

	if ($pwd2 and $pwd ne $pwd2) {
	 	$gcgi->err("Our high-paid security consultants have determined that the admin passwords you have entered do not match each other.");
	}

	$self->{cpwd} = $cgi->param('cpwd');

	if ($Girocco::Config::project_owners eq 'email') {
		$self->{email} = $gcgi->wparam('email');
		valid_email($self->{email})
			or $gcgi->err("Your email sure looks weird...?");
	}

	$self->{url} = $gcgi->wparam('url');
	if ($self->{url}) {
		valid_repo_url($self->{url})
			or $gcgi->err("Invalid URL. Note that only HTTP and Git protocol is supported. If the URL contains funny characters, contact me.");
	}

	$self->{desc} = $gcgi->wparam('desc');
	length($self->{desc}) <= 1024
		or $gcgi->err("<b>Short</b> description length &gt; 1kb!");

	$self->{README} = $gcgi->wparam('README');
	length($self->{README}) <= 8192
		or $gcgi->err("README length &gt; 8kb!");

	$self->{hp} = $gcgi->wparam('hp');
	if ($self->{hp}) {
		valid_web_url($self->{hp})
			or $gcgi->err("Invalid homepage URL. Note that only HTTP protocol is supported. If the URL contains funny characters, contact me.");
	}

	# FIXME: Permit only existing users
	$self->{users} = [grep { Girocco::User::valid_name($_) } $cgi->param('user')];

	$self->{notifymail} = $gcgi->wparam('notifymail');
	if ($self->{notifymail}) {
		(valid_email_multi($self->{notifymail}) and length($self->{notifymail}) <= 512)
			or $gcgi->err("Invalid notify e-mail address. Use mail,mail to specify multiple addresses; total length must not exceed 512 characters, however.");
	}

	$self->{notifyjson} = $gcgi->wparam('notifyjson');
	if ($self->{notifyjson}) {
		valid_web_url($self->{notifyjson})
			or $gcgi->err("Invalid JSON notify URL. Note that only HTTP protocol is supported. If the URL contains funny characters, contact me.");
	}

	$self->{notifycia} = $gcgi->wparam('notifycia');
	if ($self->{notifycia}) {
		$self->{notifycia} =~ /^[a-zA-Z0-9._-]+$/
			or $gcgi->err("Overly suspicious CIA notify project name. If it is actually valid, contact me.");
	}

	not $gcgi->err_check;
}

sub form_defaults {
	my $self = shift;
	(
		name => $self->{name},
		email => $self->{email},
		url => $self->{url},
		desc => html_esc($self->{desc}),
		README => html_esc($self->{README}),
		hp => $self->{hp},
		users => $self->{users},
		notifymail => html_esc($self->{notifymail}),
		notifyjson => html_esc($self->{notifyjson}),
		notifycia => html_esc($self->{notifycia}),
	);
}

sub authenticate {
	my $self = shift;
	my ($gcgi) = @_;

	$self->{ccrypt} or die "Can't authenticate against a project with no password";
	$self->{cpwd} ||= '';
	unless ($self->{ccrypt} eq crypt($self->{cpwd}, $self->{ccrypt})) {
		$gcgi->err("Your admin password does not match!");
		return 0;
	}
	return 1;
}

sub _setup {
	my $self = shift;
	my ($pushers) = @_;

	$self->_mkdir_forkees;

	mkdir($self->{path}) or die "mkdir $self->{path} failed: $!";
	if ($Girocco::Config::owning_group) {
		my $gid = scalar(getgrnam($Girocco::Config::owning_group));
		chown(-1, $gid, $self->{path}) or die "chgrp $gid $self->{path} failed: $!";
		chmod(02775, $self->{path}) or die "chmod 2775 $self->{path} failed: $!";
	} else {
		chmod(02777, $self->{path}) or die "chmod 2777 $self->{path} failed: $!";
	}
	system($Girocco::Config::git_bin, '--git-dir='.$self->{path}, 'init', '--bare', '--shared='.$self->shared_mode()) == 0
		or die "git init $self->{path} failed: $?";
	system($Girocco::Config::git_bin, '--git-dir='.$self->{path}, 'config', 'receive.denyNonFastforwards', 'false') == 0
		or die "disabling receive.denyNonFastforwards failed: $?";
	system($Girocco::Config::git_bin, '--git-dir='.$self->{path}, 'config', 'gc.auto', '0') == 0
		or die "disabling gc.auto failed: $?";

	# /info must have right permissions even before the fixup job,
	# and git init didn't do it for some reason.
	if ($Girocco::Config::owning_group) {
		chmod(02775, $self->{path}."/info") or die "chmod 2775 $self->{path}/info failed: $!";
	} else {
		chmod(02777, $self->{path}."/info") or die "chmod 2777 $self->{path}/info failed: $!";
	}

	$self->_properties_save;
	$self->_alternates_setup;
	$self->_ctags_setup;
	$self->_group_add($pushers);
	$self->_hooks_install;
	$self->perm_initialize;
	system($Girocco::Config::basedir . '/gitweb/genindex.sh');
}

sub premirror {
	my $self = shift;

	$self->_setup(':');
	$self->_clonep(1);
}

sub conjure {
	my $self = shift;

	$self->_setup;
	$self->_nofetch(1);
}

sub clone {
	my $self = shift;

	unlink ($self->_clonefail_path()); # Ignore EEXIST error
	unlink ($self->_clonelog_path()); # Ignore EEXIST error

	use IO::Socket;
	my $sock = IO::Socket::UNIX->new($Girocco::Config::chroot.'/etc/taskd.socket') or die "cannot connect to taskd.socket: $!";
	$sock->print("clone ".$self->{name}."\n");
	# Just ignore reply, we are going to succeed anyway and the I/O
	# would apparently get quite hairy.
	$sock->flush();
	sleep 2; # *cough*
	$sock->close();
}

sub update {
	my $self = shift;

	$self->_properties_save;
	$self->_group_update;

	my @users_add = grep { $a = $_; not scalar grep { $a eq $_ } $self->{orig_users} } $self->{users};
	my @users_del = grep { $a = $_; not scalar grep { $a eq $_ } $self->{users} } $self->{orig_users};
	$self->perm_user_add($_, Girocco::User::resolve_uid($_)) foreach (@users_add);
	$self->perm_user_del($_, Girocco::User::resolve_uid($_)) foreach (@users_del);

	1;
}

sub update_password {
	my $self = shift;
	my ($pwd) = @_;

	$self->{crypt} = scrypt($pwd);
	$self->_group_update;
}

# You can explicitly do this just on a ghost() repository too.
sub delete {
	my $self = shift;

	if (-d $self->{path}) {
		system('rm', '-rf', $self->{path}) == 0
			or die "rm -rf $self->{path} failed: $?";
	}
	$self->_group_remove;
}

sub has_forks {
	my $self = shift;

	return glob($Girocco::Config::reporoot.'/'.$self->{name}.'/*');
}


### static methods

sub get_forkee_name {
	$_ = $_[0];
	(m#^(.*)/.*?$#)[0]; #
}
sub get_forkee_path {
	my $forkee = $Girocco::Config::reporoot.'/'.get_forkee_name($_[0]).'.git';
	-d $forkee ? $forkee : '';
}

sub valid_name {
	$_ = $_[0];
	(not m#/# or -d get_forkee_path($_)) # will also catch ^/
		and (not m#\./#)
		and (not m#/$#)
		and m#^[a-zA-Z0-9+./_-]+$#;
}

sub does_exist {
	my ($name) = @_;
	valid_name($name) or die "tried to query for project with invalid name $name!";
	(-d $Girocco::Config::reporoot."/$name.git");
}


1;
