#!/bin/bash
#
# jobd - Perform Girocco maintenance jobs
#
# jobd is Girocco repositories maintenance servant; it periodically
# checks all the repositories and updates mirrored repositories and
# repacks push-repositories when needed.
#
# Execute with parameter --all-once to run only once (on all projects)
# instead of in infinite loop. Or call with parameter --one and name
# of a project (not full path, without .git suffix) to run maintenance
# only on that particular project.
#
# Use -q as VERY FIRST parameter to enable quiet mode (use in cronjobs).

. @basedir@/shlib.sh

set -e
export show_progress=1

# Lock setup

if [ -e /tmp/jobd.lock ]; then
	echo "Locked! Stale /tmp/jobd.lock?" >&2
	exit 1
fi
echo $$ >/tmp/jobd.lock
trap "rm /tmp/jobd.lock" SIGINT SIGTERM EXIT


## Single-project routine

check_one_proj()
{
	proj="$1"
	if [ ! -d "$cfg_reporoot/$proj.git" ]; then
		echo "WARNING: Skipping non-existing project $proj" >&2
		return
	fi
	if [ ! -e "$cfg_reporoot/$proj.git"/.nofetch ]; then
		"$cfg_basedir"/jobd/update.sh "$proj"
	fi
	if [ -n "$show_progress" ]; then
		"$cfg_basedir"/jobd/gc.sh "$proj"
	else
		"$cfg_basedir"/jobd/gc.sh "$proj" 2>&1 | grep -v '^Pack.*created\.$'
	fi
}


## Main loop body

check_all_projects()
{
	get_repo_list | while read proj; do
		check_one_proj "$proj"
	done
}


## Main program

if [ "$1" = "-q" ]; then
	export show_progress=
	shift
fi

case "$1" in
	"")
		while true; do
			check_all_projects
			sleep 10
		done;;
	"--all-once")
		check_all_projects;;
	"--one")
		check_one_proj "$2";;
	*)
		echo "Usage: $0 [-q] [--all-once | --one PRJNAME]" >&2
		exit 1;;
esac
